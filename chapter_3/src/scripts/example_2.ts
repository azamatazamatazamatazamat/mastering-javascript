type TType = (...args: any[]) => any;
const onceAndAfter = <FNType extends TType>(f: FNType, g: FNType) => {
	let done = false;

	return ((...args: Parameters<FNType>) => {
		if (!done) {
			done = true;
			return f(...args);
		} else {
			return g(...args);
		}
	}) as FNType;
};

const func1 = jest.fn();
const func2 = jest.fn();
const testFn = jest.fn(onceAndAfter(func1, func2));

testFn();
testFn();
testFn();
testFn();

expect(testFn).toHaveBeenCalledTimes(4);
expect(func1).toHaveBeenCalledTimes(1);
expect(func2).toHaveBeenCalledTimes(3);
