const getRandomLetter = (): string => {
	const min = "A".charCodeAt(0);
	const max = "Z".charCodeAt(0);
	return String.fromCharCode(
		Math.floor(Math.random() * (1 + max - min)) + min
	);
};

describe("getRandomLetter", function () {
	afterEach(() => {
		// so count of calls to Math.random will be OK
		jest.restoreAllMocks();
	});

	it("returns A for values ~ 0", () => {
		jest.spyOn(Math, "random").mockReturnValue(0.01);
		const letterSmall = getRandomLetter();
		expect(Math.random).toHaveBeenCalled();
		expect(letterSmall).toBe("A");
	});

	it("returns Z for values ~ 1", () => {
		jest
			.spyOn(Math, "random")
			.mockReturnValueOnce(0.0001)
			.mockReturnValueOnce(0.9149)
			.mockReturnValueOnce(0.7000)
			.mockReturnValueOnce(0.999);

		const letterBig1 = getRandomLetter();
		const letterBig2 = getRandomLetter();
		const letterBig3 = getRandomLetter();
		const letterBig4 = getRandomLetter();
		expect(Math.random).toHaveBeenCalledTimes(4);
		expect(letterBig1).toBe("A");
		expect(letterBig2).toBe("X");
		expect(letterBig3).toBe("S");
		expect(letterBig4).toBe("Z");
	});

	it("returns middle letter for values ~ 0.5", () => {
		jest
			.spyOn(Math, "random")
			.mockReturnValue(0.09);

		const letterMiddle = getRandomLetter();
		console.log(letterMiddle)

		expect(Math.random).toHaveBeenCalledTimes(1);
		expect(letterMiddle >= "A").toBeTruthy();
		expect(letterMiddle < "S").toBeTruthy();
	});
});