const getRandomLetter2 = (
	getRandomNum: () => number = Math.random.bind(Math)
): string => {
	const min = "A".charCodeAt(0);
	const max = "Z".charCodeAt(0);
	return String.fromCharCode(
		Math.floor(getRandomNum() * (1 + max - min)) + min
	);
};