type Operation = (...args: any[]) => any;
const once = <FNType extends Operation>(fn: FNType) => {
	let done = false;

	return ((...args: Parameters<FNType>) => {
		if (!done) {
			done = true;
			return fn(...args);
		}
	}) as FNType;
};

const squeak = (a: string) => console.log(a, " squeak!!");
squeak("original"); // "original squeak!!"
squeak("original"); // "original squeak!!"
squeak("original"); // "original squeak!!"

const squeakOnce = once(squeak);

squeakOnce("only once"); // "only once squeak!!"
squeakOnce("only once"); // no output
squeakOnce("only once"); // no output