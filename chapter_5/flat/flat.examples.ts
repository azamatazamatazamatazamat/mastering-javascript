import {
	flat,
	flatAll,
	flatOne,
	flatOne1,
	flatOne2,
} from "./flat";

if(0){
	const distances = [
		[0, 20, 35, 40],
		[20, 0, 10, 50],
		[35, 10, 0, 30],
		[40, 50, 30, 0],
	];

	const maxDist1 = Math.max(...distances.flat()); // 50

	const maxDist2 = distances
		.flat()
		.reduce((p, d)=>Math.max(p, d), 0); // also 50

	console.log(maxDist1, maxDist2);
// 50 50
}
if(0){
	const names = [
		"Winston Spencer Churchill",
		"Plato",
		"Abraham Lincoln",
		"Socrates",
		"Charles Darwin",
	];

	const lastNames = names.flatMap((x)=>{
		const s = x.split(" ");
		return s.length === 1 ? [] : s.splice(1);
	});
	console.log(lastNames);
// [ 'Spencer', 'Churchill', 'Lincoln', 'Darwin' ]
}
if(1){
	const gettysburg = [
		"Four score and seven years ago our fathers",
		"brought forth, on this continent, a new nation,",
		"conceived in liberty, and dedicated to the",
		"proposition that all men are created equal.",
		"Now we are engaged in a great civil war,",
		"testing whether that nation, or any nation",
		"so conceived and so dedicated, can long endure.",
		"We are met on a great battle field of that",
		"war. We have come to dedicate a portion of",
		"that field, as a final resting place for",
		"those who here gave their lives, that that",
		"nation might live. It is altogether",
		"fitting and proper that we should do this.",
		"But, in a larger sense, we cannot dedicate,",
		"we cannot consecrate, we cannot hallow,",
		"this ground.",
		"The brave men, living and dead, who",
		"struggled here, have consecrated it far",
		"above our poor power to add or detract.",
		"The world will little note nor long",
		"remember what we say here, but it can",
		"never forget what they did here.",
		"It is for us the living, rather, to be",
		"dedicated here to the unfinished work",
		"which they who fought here have thus far",
		"so nobly advanced.",
		"It is rather for us to be here dedicated",
		"to the great task remaining before us—",
		"that from these honored dead we take",
		"increased devotion to that cause for",
		"which they here gave the last full",
		"measure of devotion— that we here highly",
		"resolve that these dead shall not have",
		"died in vain— that this nation, under",
		"God, shall have a new birth of freedom-",
		"and that government of the people, by",
		"the people, for the people, shall not",
		"perish from the earth.",
	];
	console.log(gettysburg.map((x)=>x.split(" ")));
	console.table(
		gettysburg.flatMap((s: string)=>s.split(" "))
	); // 270 ...not 272?
}
if(0){
	// const a = [[1, 2], [3, 4, [5, 6, 7]], 8, [[[9]]]];
	// console.log(flatAll(a));
	// console.log(flat(a));
	// console.log(flat(a, 1));
	// console.log(flat(a, 2));
	// console.log(flat(a, Infinity));
}
