const apiAnswer = [
	{
		country: "AR",
		name: "Argentine",
		states: [
			{
				state: "1",
				name: "Buenos Aires",
				cities: [{city: 3846864, name: "Lincoln"}],
			},
		],
	},
	{
		country: "GB",
		name: "Great Britain",
		states: [
			{
				state: "ENG",
				name: "England",
				cities: [{city: 2644487, name: "Lincoln"}],
			},
		],
	},
	{
		country: "US",
		name: "United States of America",
		states: [
			{
				state: "CA",
				name: "California",
				cities: [{city: 5072006, name: "Lincoln"}],
			},
			{
				state: "RI",
				name: "Rhode Island",
				cities: [{city: 8531960, name: "Lincoln"}],
			},
			{
				state: "VA",
				name: "Virginia",
				cities: [{city: 4769608, name: "Lincolnia"}],
			},
			{
				state: "MI",
				name: "Michigan",
				cities: [{city: 4999311, name: "Lincoln Park"}],
			},
			{
				state: "NE",
				name: "Nebraska",
				cities: [{city: 5072006, name: "Lincoln"}],
			},
			{
				state: "IL",
				name: "Illinois",
				cities: [
					{city: 4899911, name: "Lincoln Park"},
					{city: 4899966, name: "Lincoln Square"},
				],
			},
		],
	},
];

console.log(
	apiAnswer
		.map((x)=>x.states)
		.flat()
		.map((y)=>y.cities)
		.flat()
);

console.log(
	apiAnswer
		.flatMap((x)=>x.states)
		.flatMap((y)=>y.cities)
);
