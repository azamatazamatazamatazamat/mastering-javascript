const flatAll = (arr: any[]): any[] =>
	arr.reduce(
		(arr: any[], item: any) =>
			arr.concat(Array.isArray(item) ? flatAll(item) : item),
		[]
	);

const nestedArray = [1, 2, 3, 4, [21, 22, [31, 32, 33], 23], 5, 6, 7, 8, 9];
console.log(flatAll(nestedArray))