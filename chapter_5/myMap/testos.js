const myMap = (arr,fn)=>
	arr.reduce((x,y)=>x.concat(fn(y)),[]);

const dup = (x)=>2 * x;

const myArray = [22, 9, 60, 12, 4, 56];
console.log(myMap(myArray,dup));