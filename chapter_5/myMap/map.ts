const myMap = <T, R>(arr: T[], fn: (x: T) => R): R[] =>
  arr.reduce(
    (arr: R[], item: T): R[] => arr.concat([fn(item)]),
    [] as R[]
  );

export { myMap };
