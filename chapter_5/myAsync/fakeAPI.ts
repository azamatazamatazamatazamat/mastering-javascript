const fakeAPI = <T>(delay: number, value: T): Promise<T> =>
	new Promise((resolve) =>
		setTimeout(() => resolve(value), delay)
	);

const useResult = (x: any): void =>
	console.log(new Date(), x);

(async () => {
	console.log("START");
	console.log(new Date());
	const result = await fakeAPI(5000, 229);
	useResult(result);
	console.log("END");
})();