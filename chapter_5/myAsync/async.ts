import any = jasmine.any;
import * as constants from "constants";

const fakeAPI = <T>(delay: number, value: T): Promise<T> => {
	return new Promise((resolve) => {
		return setTimeout(() => resolve(value), delay);
	});
}

const useResult = (x: any): void =>
	console.log(new Date(), x);

type TPromise = Promise<any | void>;
const forEachAsync = <T>(arr: T[], func: (x: T) => any): TPromise => {
	return arr.reduce(
		(promise: TPromise, item: T) => {
			return promise.then(() => {
				return func(item);
			});
		},
		Promise.resolve()
	);
}

const mapAsync = <T, R>(
	arr: T[],
	func: (x: T) => Promise<R>
) => {
	return Promise.all(arr.map(func));
}

const fakeFilter = (value: number): Promise<boolean> =>
	new Promise((resolve) =>
		setTimeout(() => resolve(value % 2 === 0), 5000)
	);

const filterAsync = <T>(
	arr: T[],
	fn: (x: T) => Promise<boolean>
) => {
	return Promise.all(arr.map(item => fn(item)))
		.then((arr2) => {
			return arr.filter((v, i) => Boolean(arr2[i]))
		});
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (0) {
	(async () => {
		console.log("START");
		console.log(new Date());
		const result = await fakeAPI(1000, 229);
		useResult(result);
		console.log("END");
	})();
// START
// 2022-10-29T01:28:12.986Z
// 2022-10-29T01:28:13.989Z 229
// END
}
if (0) {
	(async () => {
		console.log("START SEQUENCE");
		const x1 = await fakeAPI(1000, 1);
		useResult(x1);
		const x2 = await fakeAPI(2000, 2);
		useResult(x2);
		const x3 = await fakeAPI(3000, 3);
		useResult(x3);
		const x4 = await fakeAPI(4000, 4);
		useResult(x4);

		console.log("END SEQUENCE");
	})();
// START SEQUENCE
// 2022-10-29T01:32:11.671Z 1
// 2022-10-29T01:32:13.677Z 2
// 2022-10-29T01:32:16.680Z 3
// 2022-10-29T01:32:20.683Z 4
// END SEQUENCE
}
if (0) {
	(() => {
		console.log("START FOREACH");
		[1, 2, 3, 4].forEach(async (n) => {
			const x = await fakeAPI(n * 1000, n);
			useResult(x);
		});

		console.log("END FOREACH");
	})();
	/*
	START FOREACH
	END FOREACH
	2022-10-29T01:34:06.287Z 1
	2022-10-29T01:34:07.287Z 2
	2022-10-29T01:34:08.286Z 3
	2022-10-29T01:34:09.286Z 4
	*/
}
if (0) {
	(async () => {
		console.log("START FOREACH VIA REDUCE");
		const innerFun = async (n: number) => {
			// const x = await fakeAPI(n * 1000, n);
			const x = await (<T>(delay: number, value: T): Promise<T> => {
				return new Promise((resolve) => {
					return setTimeout(() => resolve(value), delay);
				});
			})(n * 1000, n);

			useResult(x);
		}
		await forEachAsync([1, 2, 3, 4], innerFun);
		console.log("END FOREACH VIA REDUCE");
	})();
	/*
	START FOREACH VIA REDUCE
		2022-10-29T01:42:09.385Z 1
		2022-10-29T01:42:11.388Z 2
		2022-10-29T01:42:14.391Z 3
		2022-10-29T01:42:18.392Z 4
	END FOREACH VIA REDUCE
	*/
}
if (0) {
	(async () => {
		console.log("START MAP");
		console.log("----------------------------------")
		const CB = async (ss: number) => {
			const x = await (<T>(delay: number, value: T): Promise<T> => {
				return new Promise((resolve) => {
					return setTimeout(() => resolve(value), delay);
				});
			})(ss * 1000, ss);
			return x * 10;
		}

		// IIFE
		const mapped = await (<T, R>(
			arr: T[],
			func: (ss: T) => Promise<R>
		) => {
			// return Promise.all(arr.map(func));
			const newArray = arr.map(item => {
				return func(item);
			})

			for (const newArrayElement of newArray) {
				(async () => {
					console.log(await newArrayElement)
				})()
			}

			return Promise.all(newArray);
		})([7, 1, 2, 3, 4, 5, 6], CB)


		useResult(mapped);
		console.log("----------------------------------")
		console.log("END MAP");
	})();
	/*
	START MAP
	2022-10-29T01:47:06.726Z [ 10, 20, 30, 40 ]
	END MAP
	*/
}
if (0) {
	(async () => {
		console.log("START FILTER");
		/** 1 вариант */
		if (0) {
			const filtered = await filterAsync(
				new Array(100).fill(1).map((item, index) => item + index),
				async (n) => {
					return new Promise((resolve) =>
						setTimeout(() => resolve(n % 2 === 0), 5000)
					);
					// const x = await fakeFilter(n);
					// return x;
				}
			);
			useResult(filtered);
		}
		if (1) {
			/** 2 вариант */
			const myArray = new Array(100).fill(1).map((item, index) => item + index)

			const newMyArray = myArray.map(item => {
				return new Promise((resolve) =>
					setTimeout(() => resolve(item % 2 === 0), 3000)
				);
			});

			const filtered = await Promise.all(newMyArray)
				.then((arr2) => {
					return myArray.filter((v, i) => Boolean(arr2[i]))
				});
			useResult(filtered);
		}
		console.log("END FILTER");
	})();
	/*
	START FILTER
	2022-10-29T01:56:19.798Z [ 2, 4 ]
	END FILTER
	*/
}


const forEachAsync2 = <T>(arr: T[], func: (x: T) => any): TPromise => {
	return arr.reduce(
		(promise: TPromise, item: T) => {
			return promise.then(() => {
				return func(item);
			});
		},
		Promise.resolve()
	);
}

const fakeSum = (
	value1: number,
	value2: number
): Promise<number> =>
	new Promise((resolve) =>
		setTimeout(() => resolve(value1 + value2), 1000)
	);
//-----------------------------------
// (async ()=>{
// 	return await fakeSum(10,2)
// })().then(console.log)
//------------------------------
// new Promise(resolve => {
// 	resolve(fakeSum(10,4))
// }).then(console.log)
//------------------------------
// const aza = new Promise(resolve => {
// 	resolve(fakeSum(10,4))
// });//.then(console.log)
//
// (async ()=>{
// 	const aza2 = await aza
// 	console.log(aza2)
// })()//


//-----------------------------------
const reduceAsync = <T, R>(
	arr: T[],
	func: (acc: R, val: T) => Promise<R>,
	init: R
) =>
	Promise.resolve(init).then((accumThen) =>
//---------------------------------------------------
			/** 1 вариант */
			// forEachAsync(arr, async(v: T)=>{
			// 	accum = await fn(accum, v);
			// }).then(()=>accum)
//---------------------------------------------------
			/** 2 вариант */
			arr.reduce((promise: TPromise, item: T) => {
					return promise.then(() => {
						return (async () => {
							/** 1 вариант */
							accumThen = await func(accumThen, item);
						})()
					});
				},
				Promise.resolve()
			).then(() => accumThen)
//---------------------------------------------------
	);

if(0)
	(async () => {
		console.log("START REDUCE");
		const summed = await reduceAsync(
			[1, 2, 3, 4, 5, 6],
			//----------------------------------------
			async (_accum, n) => {
				const accum = await _accum;
				const x = await fakeSum(accum, n);
				useResult(`accum=${accum} value=${x} `);
				return x;
			},
			//----------------------------------------
			0
		);
		useResult(summed);
		console.log("END REDUCE");
	})();

/*
START REDUCE
2022-10-29T02:04:20.862Z accum=0 value=1
2022-10-29T02:04:21.864Z accum=1 value=3
2022-10-29T02:04:22.865Z accum=3 value=6
2022-10-29T02:04:23.866Z accum=6 value=10
2022-10-29T02:04:23.866Z 10
END REDUCE
*/

const reduceAsync2 = <T,R>(arr: T[],init: R) =>
	Promise.resolve(init).then((accumThen) =>
		arr.reduce((promise: TPromise, item: T) => {
				return promise.then(() => {
					return (async () => {
						const accum = accumThen;
						accumThen = await (async() => {
							return await fakeSum(
								accum as number,
								item as number
							) as R
						})();
						useResult(`accum=${accumThen} value=${item} `);
					})()
				});
			},
			Promise.resolve()
		).then(() => accumThen)
//---------------------------------------------------
	);

(async () => {
	console.log("START REDUCE");

	const summed = await reduceAsync2(
		[1, 2, 3, 4, 5, 6],
		//----------------------------------------
		0
	);

	useResult(summed);
	console.log("END REDUCE");
})();


export {fakeFilter, fakeSum, fakeAPI, useResult};
export {mapAsync, forEachAsync, reduceAsync, filterAsync};

