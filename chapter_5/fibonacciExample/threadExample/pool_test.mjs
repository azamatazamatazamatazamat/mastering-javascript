import worker from "worker_threads";
// import {workerCall} from "./pool.mjs";

const pool = [];
const workerCall = (filename,value)=>{
	// console.log("start")
	let available = pool
		.filter((item_v)=>{
			return !item_v.inUse;
		})
		.find((item_x)=>{
			/** переиспользование файла */
			console.log("переиспользование файла ",item_x.filename);
			return  item_x.filename === filename;
		});

	if(available === undefined){
		console.log("CREATING", filename, value);
		available = {
			worker:new worker.Worker(filename),
			filename,
			value,
			inUse:true,
		};
		pool.push(available);
	}else{
		console.log("REUSING","\t", filename,"\t", available.value);
	}

	return new Promise((resolve)=>{
		available.inUse = true;
		available.worker.on("message",(x)=>{
			resolve(x);
			available.inUse = false;
			// console.log("RESOLVING", filename, value, x);
		});
		available.worker.postMessage(value);
	});
};
//--------------------------------------------------------------------
const FIB_WORKER = "./fib_worker.js";
const RANDOM_WORKER = "./random_worker.mjs";

console.log("++++++++++++++++++++++++++++++++++++++++")
const showResult = (s)=>(x)=>console.log("\t",s," _ ",x);
workerCall(FIB_WORKER,44).then(showResult("fib(44)"));
workerCall(RANDOM_WORKER,1000).then(showResult("random"));
workerCall(FIB_WORKER,35).then(showResult("fib(35)"));
workerCall(RANDOM_WORKER,3000).then(showResult("random"));
workerCall(FIB_WORKER,20).then(showResult("fib(20)"));
/** переиспользование файла */
workerCall(FIB_WORKER,10).then((x)=>{
	console.log("переиспользование fib(10)",x);
	workerCall(FIB_WORKER,11).then((y)=>
		console.log("переиспользование fib(11) ",y)
	);
});
workerCall(RANDOM_WORKER,2000).then(showResult("random"));
