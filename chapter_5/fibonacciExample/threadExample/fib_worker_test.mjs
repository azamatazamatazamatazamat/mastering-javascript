// const { Worker } = require('worker_threads');
import {Worker} from 'worker_threads';
const worker = new Worker("./threadExample/fib_worker.js");

console.log("START");
worker.postMessage(40);
console.log("END");

worker.on("message",(msg)=>{
	console.log("MESSAGE",msg);
	worker.terminate();
});
