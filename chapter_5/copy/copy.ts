const myObj = {
	fk: 22,
	st: 15,
	desc: "couple",
	sherk: {
		tt: "123",
		aa: "12345",
		park: {
			tt2: "tt2_123",
			aa2: "aa2_12345",
		}
	}
};
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if (0) {
	const copy = Object.create(Object.getPrototypeOf(myObj));
	/** Метод Object.getPrototypeOf() в JavaScript используется для
	 * получения прототипа указанного объекта. Прототип объекта - это
	 * объект, который используется в качестве основы для создания
	 * других объектов через механизм наследования в JavaScript. */
	console.log(Object.getPrototypeOf(myObj))//[Object: null prototype] {}
	console.log(copy)//{}
}
if (0) {
	const myObject = Object.create({}, {
		name: {
			value: 'John',
			writable: true, // можно ли изменять значение свойства
			enumerable: true, // свойство будет видимым при переборе в цикле
			configurable: true // можно ли удалить или изменить свойство
		},
		age: {
			value: 30
		},
		sayHello: {
			value: function () {
				console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
			}
		}
	});
	myObject.name = "aza"
	console.log(myObject.name); // Выведет: 'John'
	console.log(myObject.age); // Выведет: 30
	myObject.sayHello(); // Выведет: 'Hello, my name is John and I am 30 years old.'
}
if (0) {
	/** Метод Object.defineProperty() в JavaScript используется для
	 *  добавления или изменения свойства в объекте с определенными
	 *  характеристиками.
Object.defineProperty(obj, prop, descriptor)
  obj: Объект, в котором будет определено свойство.
  prop: Имя свойства, которое будет добавлено или изменено.
  descriptor: Объект, содержащий характеристики свойства
 (например, значение, доступ, перечислимость и конфигурируемость).*/
	const obj = Object.getPrototypeOf(myObj);
	// const obj = Object.create(Object.getPrototypeOf(myObj));
	// const obj = Object.create();
	Object.defineProperty(obj, 'name', {
		value: 'John', // Значение свойства
		writable: true, // Можно ли изменять значение свойства (по умолчанию false)
		enumerable: true, // Свойство будет видимым при перечислении (по умолчанию false)
		configurable: true // Можно ли удалить или изменить свойство (по умолчанию false)
	});
	obj.name = "aza";
	console.log(obj.name); // Выведет: 'John'
}
if (1) {
	const objCopy = <T>(obj: T): T => {
		// const copy = Object.create(Object.getPrototypeOf(obj));
		const copy = Object.getPrototypeOf(obj);
		Object.keys(obj).forEach((prop: string) =>
			Object.defineProperty(
				copy,
				prop,
				Object.getOwnPropertyDescriptor(obj, prop) as string
			)
		);
		return copy;
	};

	const myCopy = objCopy(myObj);
	console.log(myObj, myCopy); // {fk: 22, st: 12, desc: "couple"}, twice
}
