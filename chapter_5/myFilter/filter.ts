const serviceResult = {
	accountsData: [
		{id: "F220960K", balance: 1024},
		{id: "S120456T", balance: 2260},
		{id: "J140793A", balance: -38},
		{id: "M120396V", balance: -114},
		{id: "A120289L", balance: 55000},
	],
};
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(0){
	const delinquent = serviceResult.accountsData.filter(
		(v)=>v.balance < 0
	);
	console.log(delinquent);
	const delinquentIds = delinquent.map((v)=>v.id);
	console.log(delinquentIds);
}
if(0){
	const delinquentIds2 = serviceResult.accountsData
		.filter((v)=>v.balance < 0)
		.map((v)=>v.id);
	console.log(delinquentIds2);
}
if(1){
	const myFilter = <T>(arr: T[], func: (x: T)=>boolean)=>{
		return arr.reduce((accArr: T[], item: T)=>{
			return (func(item) ? accArr.concat(item) : accArr);
		}, []);
	}
	//---------------------------------------------------------
	const balance = (v: { balance: number; })=>{
		return v.balance < 0;
	}
	myFilter([
		{id: "F220960K", balance: 1024},
		{id: "S120456T", balance: 2260},
		{id: "J140793A", balance: -38},
		{id: "M120396V", balance: -114},
		{id: "A120289L", balance: 55000},
	], balance).forEach(item=>console.log(item));
}


