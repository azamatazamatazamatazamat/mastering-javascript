if(0){
	/** RECORD RECORD RECORD RECORD RECORD RECORD RECORD RECORD RECORD */
	/** RECORD RECORD RECORD RECORD RECORD RECORD RECORD RECORD RECORD */
	/** RECORD RECORD RECORD RECORD RECORD RECORD RECORD RECORD RECORD */
	/** RECORD RECORD RECORD RECORD RECORD RECORD RECORD RECORD RECORD */
	/** В TypeScript, Record это утилита для создания типов объектов, где ключи
   	и значения имеют заданные типы. Record<K, T> принимает два типа:
    K: тип ключей объекта.
    T: тип значений объекта.
	 */
	if(0){
		type PhonePrices = Record<string,number>;
		const phonePrices:PhonePrices = {
			iPhone:999,
			Galaxy:799,
			Pixel:699
		};
		console.log(phonePrices.iPhone); // Output: 999
	}
//*************************************************************************************
	if(0){
		interface Person{
			name:string;
			age:number;
		}

		type People = Record<string,Person>;
		const people:People = {
			alice:{name:'Alice',age:30},
			bob:{name:'Bob',age:25}
		};
		console.log(people.alice.name); // Output: Alice
	}
//*************************************************************************************
	if(0){
		type PhoneModels = 'iPhone' | 'Galaxy' | 'Pixel';
		type PhonePrices = Record<PhoneModels,number>;
		const phonePrices:PhonePrices = {
			iPhone:999,
			Galaxy:799,
			Pixel:699
		};
		console.log(phonePrices.Galaxy); // Output: 799
	}
}
if(1){
	/** RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE */
	/** RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE */
	/** RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE */
	/** RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE RETURNTYPE */
	/** ReturnType — это утилита, встроенная в TypeScript, которая позволяет извлечь
	 * тип возвращаемого значения из функции. Это полезно, когда вам нужно повторно
	 * использовать тип возвращаемого значения функции или когда вы хотите создать тип,
	 * который зависит от типа возвращаемого значения другой функции.*/
	if(0){
		function getUser(){
			return {
				name:'Alice',
				age:30
			};
		}

		// Используем ReturnType для извлечения типа возвращаемого значения функции getUser
		type User = ReturnType<typeof getUser>;
		const user:User = {
			name:'Alice',
			age:30
		};
		console.log(user.name); // Output: Alice
	}
	if(0){
		/** Если у вас есть функция, возвращающая другую функцию,
		 * вы также можете использовать ReturnType для извлечения типа: */
		function createMultiplier(factor:number){
			return (input:number)=>input * factor;
		}

		// console.log(createMultiplier(10)(10))
		// Извлекаем тип возвращаемой функции
		type T_Multiplier = ReturnType<typeof createMultiplier>;
		const double:T_Multiplier = createMultiplier(2);
		console.log(double(5)); // Output: 10
	}
	if(0){
		function fetchData(){
			return new Promise<{message:string}>((resolve)=>{
				setTimeout(
					()=>resolve({message:'you die'}),
					1000);
			});
		}

// Извлекаем тип возвращаемого значения функции fetchData
		type T_FetchDataType = ReturnType<typeof fetchData>;

// Пример использования
		const dataPromise:T_FetchDataType = fetchData();
		dataPromise.then(result=>console.log(result.message)); // Output: Sample data
	}
	if(0){
		/**
		 Ограничения ReturnType
		Есть некоторые ограничения, которые стоит учитывать при использовании ReturnType:

    Не может использоваться с перегруженными функциями: ReturnType может работать только с конкретными сигнатурами функций, а не с перегрузками функций.
    Не работает с конструкторами: ReturnType не может извлекать типы из конструкторов классов.
		class Example {
    	constructor(public name: string) {}
		}
		type ExampleConstructorReturnType = ReturnType<typeof Example>; // Ошибка: 'Example' refers to a value, but is being used as a type here.
		 */
	}
	if(0){
//----------------------------------------------------------
		let changeSign = (a:number):number=>-a;

		function subtract(a:number,b:number):number{
			if(b === 0){
				throw new Error("Нельзя делить на ноль!");
			}else{
				b = changeSign(b);
				return a + b;
			}
		}

//----------------------------------------------------------
		function addLoggingExample<T extends (...args:any[])=>any>(
			func:T,
			logger = console.log.bind(console)
		):(...args:Parameters<T>)=>ReturnType<T>{
			return (...args:Parameters<T>):ReturnType<T>=>{
				logger(`entering ${func.name}(${args})`);
				try{
					const valueToReturn = func(...args);
					logger(`exiting  ${func.name}=>${valueToReturn}`);
					/** возвращаем переданную функцию в addLoggingExample */
					return valueToReturn;
				}catch(thrownError){
					logger(`exiting  ${func.name}=>threw ${thrownError}`);
					throw thrownError;
				}
			};
		}

//----------------------------------------------------------
		const newFunc1 = addLoggingExample(subtract);
		newFunc1(8,3);
//----------------------------------------------------------
	}
	if(1){
		const memoize4 = <T extends (...x:any[])=>any>(
			fn:T
		):((...x:Parameters<T>)=>ReturnType<T>)=>{
			const cache = {} as Record<string,ReturnType<T>>;
			return (...args)=>{
				const strX = JSON.stringify(args);
				console.log("strX ",strX);
				return strX in cache
					? cache[strX]
					: (cache[strX] = fn(...args));
			};
		};

		function sum(a:number,b:number):number{
			return a + b;
		}

		let total = memoize4(sum);
		console.log(total(2,2))

		total = sum
		// console.log(total(2,2))

		total = sum
		// console.log(total(2,3))

		total = sum
		// console.log(total(4,3))


		// function factorial(n: number): number{
		// 	if(n === 0)
		// 		return  1
		//
		// 	const total = n + factorial(n - 1);
		// 	console.log("total ",total,"\t",n);
		// 	return total;
		// }
		//
		// console.log("start")
		// const testMemoFact = memoize4(factorial);
		// console.log(testMemoFact(2));
		// console.log(testMemoFact(5));
		// console.log(testMemoFact(4));
		// console.log(testMemoFact(3));
		// console.log(testMemoFact(10));
	}
	if(0){
		const fib = (n:number):number=>{
			if(n == 0)
				return 0;
			if(n == 1)
				return 1;
			return fib(n - 2) + fib(n - 1);
		}


	}
}
if(0){
	/** EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS */
	/** EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS */
	/** EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS */
	/** EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS EXTENDS */
	/** extends можно использовать для ограничения обобщенных типов, указывая,
	 *  что тип должен соответствовать определенному интерфейсу или классу.*/
	if(0){
		interface Lengthwise{
			length:number;
		}

		function logLength<T extends Lengthwise>(item:T):void{
			console.log(item.length);
		}

		logLength({length:10}); // Output: 10
		logLength('Hello, TypeScript!'); // Output: 17
		// logLength(123); // Ошибка: Тип 'number' не удовлетворяет ограничению 'Lengthwise'
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
}
if(0){
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
}
if(0){
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
}
if(0){
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
}
if(0){
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
	if(0){
	}
}
