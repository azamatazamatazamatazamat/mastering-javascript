type Predicate<A> = (x: A) => boolean;
/*
const not = (fn) => (...args) => !fn(...args);
*/

const not = <T extends (...args:any[])=>boolean>(func:T)=>{
	return (...args:Parameters<T>):boolean=>{
		return !func(...args);
	}
}

/*
const filterNot = (arr) => (fn) => arr.filter(not(fn));
*/

const filterNot = <A,T extends (x:A)=>boolean>(arr:A[])=>{
	return (fn:T):A[]=>{
		return arr.filter(not((y)=>fn(y)));
	}
}

export {not,filterNot};
