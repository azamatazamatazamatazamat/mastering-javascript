import {not,filterNot} from "./not";

type AccountData = {
	id:string;
	balance:number;
};

const serviceResult = {
	accountsData:[
		{
			id:"F220960K",
			balance:1024,
		},
		{
			id:"S120456T",
			balance:2260,
		},
		{
			id:"J140793A",
			balance:-38,
		},
		{
			id:"M120396V",
			balance:-114,
		},
		{
			id:"A120289L",
			balance:55000,
		},
	],
};
const isNegativeBalance = (item:AccountData)=>item.balance < 0;
//################################################################
if(0){
	const delinquent = serviceResult.accountsData.filter(
		(item)=>item.balance < 0
	);
	console.log(JSON.stringify(delinquent,null,2));
}
if(0){
	const notDelinquent = serviceResult.accountsData.filter(
		(item)=>item.balance >= 0
	);
	console.log(JSON.stringify(notDelinquent,null,2));
}
if(0){
	const notDelinquent2 = serviceResult.accountsData.filter(
		(item)=>!(item.balance < 0)
	);
	console.log(JSON.stringify(notDelinquent2,null,2));
}
if(0){
	const delinquent2 = serviceResult.accountsData.filter(isNegativeBalance);
	console.log(JSON.stringify(delinquent2,null,2));
}
if(0){
	const notDelinquent3 = serviceResult.accountsData.filter(
		not(isNegativeBalance)
	);
	console.log(JSON.stringify(notDelinquent3,null,2));
}
if(1){
	const newArr = filterNot(serviceResult.accountsData)(isNegativeBalance);
	console.log(JSON.stringify(newArr,null,2));
}