import {invert} from "./invert";

if(0){
	const spanishComparison = (a: string, b: string): number=>a.localeCompare(b, "es");
	const palabras = ["ñandú", "oasis", "mano", "natural", "mítico", "musical",];
	palabras.sort(spanishComparison);
	console.log(palabras)
	// ["mano", "mítico", "musical", "natural", "ñandú", "oasis"]
	palabras.sort(invert(spanishComparison));
	console.log(palabras);
	// ["oasis", "ñandú", "natural", "musical", "mítico", "mano"]
}
if(1){
	const invertNum = <T extends (...args: number[]) => number>(fn: T) =>
		(...args: Parameters<T>): number =>
			-fn(...args);
	const numbersComparison = (a: number, b: number): number => a - b;
	const palabras = [2, 1, 6, 4, 3, 5];
	palabras.sort(numbersComparison);
	console.log(palabras);
	// [ 1, 2, 3, 4, 5, 6 ]
	palabras.sort(invertNum(numbersComparison));
	console.log(palabras);
	// [ 6, 5, 4, 3, 2, 1 ]
}
