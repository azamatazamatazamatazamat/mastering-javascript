import {addLogging2, addLogging, addLogging3} from "./logging";
import {subtract} from "./logging.examples";

/** данный тест провалится! */
describe("a logging function", function(){
	afterEach(()=>{
		// so count of calls to Math.random will be OK
		jest.restoreAllMocks();
	});

	/** пример для jest.spyOn*/
	// test('проверка вызова console.log', () => {
	// 	const consoleSpy = jest.spyOn(global.console, "log");
	// 	// некоторый код, который использует console.log
	// 	console.log("Hello, world!");
	// 	expect(consoleSpy).toHaveBeenCalledWith("Hello, world!");
	// 	expect(consoleSpy).toHaveBeenCalledTimes(1);
	// 	// Восстанавливаем оригинальную функцию console.log после завершения теста
	// 	consoleSpy.mockRestore();
	// });

	test("должен войти в систему дважды с " +
		"хорошо работающими функциями", ()=>{
		jest.spyOn(global.console, "log");

		let something = (a: number, b: number): string=>
			`result=${a}:${b}`;

		something = addLogging2(something);

		something(22, 9);

		expect(global.console.log).toHaveBeenCalledTimes(2);
		expect(global.console.log).toHaveBeenNthCalledWith(
			1,
			"entering something(22,9)"
		);
		expect(global.console.log).toHaveBeenNthCalledWith(
			2,
			"exiting  something=>result=22:9"
		);
	});

	it("должен сообщить о выброшенном исключении", ()=>{
		jest.spyOn(global.console, "log");

		let subtractZero = (x: number)=>subtract(x, 0);
		subtractZero = addLogging2(subtractZero);

		expect(()=>subtractZero(10)).toThrow();
		expect(global.console.log).toHaveBeenCalledTimes(2);
		expect(global.console.log).toHaveBeenNthCalledWith(
			1,
			"entering subtractZero(10)"
		);
		expect(global.console.log).toHaveBeenNthCalledWith(
			2,
			"exiting  subtractZero=>threw Error: We don't subtract zero!"
		);
	});
});
// ********************************************************************************
/** данный тест НЕ провалится! */
describe("addLogging3()", function(){
	it("should call the provided logger", ()=>{
		const logger = jest.fn();

		let something = (a: number, b: number): string=>
			`result= ***  ${a}:${b} ***`;

		something = addLogging3(something, logger);
		something(22, 9);

		expect(logger).toHaveBeenCalledTimes(2);
		expect(logger).toHaveBeenNthCalledWith(
			1,
			/** something - название функции получаемой через
			 * fn.name в функции addLogging3 */
			"entering something(22,9)"
		);
		expect(logger).toHaveBeenNthCalledWith(
			2,
			"exiting  something=>result= ***  22:9 ***"
		);
	});

	it("a throwing function should be reported", () => {
		const logger = jest.fn();

		let thrower = () => {
			throw "CRASH!";
		};

		thrower = addLogging3(thrower, logger);

		try {
			thrower();
		} catch (e) {
			expect(logger).toHaveBeenCalledTimes(2);
			expect(logger).toHaveBeenNthCalledWith(
				1,
				"entering thrower()"
			);
			expect(logger).toHaveBeenNthCalledWith(
				2,
				"exiting  thrower=>threw CRASH!"
			);
		}
	});
});