/*
function addLogging(fn) {
  return (...args) => {
    console.log(`entering ${fn.name}(${args})`);
    const valueToReturn = fn(...args);
    console.log(`exiting  ${fn.name}=>${valueToReturn}`);
    return valueToReturn;
  };
}
*/

// https://spin.atomicobject.com/2019/01/11/typescript-higher-order-functions/
/**
1.		T extends (...args: any[]) => any: Это ограничение типа означает,
 			что тип T должен быть функцией, которая принимает любое количество аргументов
 			и возвращает значение.

2.		fn: T: Функция addLogging принимает функцию fn с типом T,
 			которая будет декорирована (расширена с добавлением логирования).

3.		(...args: Parameters<T>): ReturnType<T>: Эта стрелочная функция также
 			имеет обобщенный тип, который соответствует типу функции fn.
			Она принимает аргументы того же типа, что и функция fn, и возвращает то
 			же значение, что и функция fn.

4.		console.log(entering ${fn.name}(${args}));: В этой строке логируется вход в
 			функцию fn с передачей аргументов.

5.		const valueToReturn = fn(...args);: Здесь вызывается функция fn с передачей
 			аргументов, и результат сохраняется в valueToReturn.

6.		console.log(exiting ${fn.name}=>${valueToReturn});: Здесь логируется выход
 			из функции fn с возвращаемым значением.

7.		return valueToReturn;: Функция возвращает то же значение, которое вернула
 			функция fn.
 */
/** не удачное решение, которое вызывает сложности при тестировании */
function addLogging<T extends (...args: any[])=>any>(fn: T):
	(...args: Parameters<T>)=>ReturnType<T>{
//---------------------------------------------------------
	return (...args: Parameters<T>): ReturnType<T>=>{
		console.log("+++++++++++++++++++++++++++++")
		console.log(`entering: ${fn.name}(${args})`);
		const valueToReturn = fn(...args);
		console.log(`exiting: ${fn.name} => ${valueToReturn}`);
		console.log("----------------------------")
		return valueToReturn;
	};
//---------------------------------------------------------
}
/** не удачное решение, которое вызывает сложности при тестировании */
function addLogging2<T extends (...args: any[])=>any>(
	fn: T
): (...args: Parameters<T>)=>ReturnType<T>{
	return (...args: Parameters<T>): ReturnType<T>=>{
// --------------------------------------------------------
		console.log(`entering: ${fn.name}(${args})`);
// --------------------------------------------------------
		try{
			const valueToReturn = fn(...args);
			console.log(`exiting: ${fn.name}=>${valueToReturn}`);
			return valueToReturn;
		}catch(thrownError){
			console.log(
				`exiting: ${fn.name}=>threw ${thrownError}`
			);
			throw thrownError;
		}
	};
}
/** удачное решение */
function addLogging3<T extends (...args: any[])=>any>(
	fn: T,
	logger = console.log.bind(console)
): (...args: Parameters<T>)=>ReturnType<T>{
	return (...args: Parameters<T>): ReturnType<T>=>{
		logger(`entering ${fn.name}(${args})`);
		try{
			const valueToReturn = fn(...args);
			logger(`exiting  ${fn.name}=>${valueToReturn}`);
			/** возвращаем переданную функцию в addLogging3 */
			return valueToReturn;
		}catch(thrownError){
			logger(`exiting  ${fn.name}=>threw ${thrownError}`);
			throw thrownError;
		}
	};
}


export {addLogging, addLogging2, addLogging3};

/**
 * ReturnType
 * ReturnType
 * ReturnType
 * ReturnType
 * ReturnType

 `ReturnType<T>` - это встроенный обобщенный тип в TypeScript, который извлекает тип
 возвращаемого значения из типа функции `T`.

Например, если у вас есть функция:

		```typescript
		function greet(): string {
				return "Hello";
		}
		```

Вы можете использовать `ReturnType` следующим образом:

		```typescript
		type GreetReturnType = ReturnType<typeof greet>; // string
		```

Таким образом, `GreetReturnType` будет типом `"string"`, потому что функция `greet`
 возвращает строку.

В контексте вашей функции `addLogging`, `ReturnType<T>` используется для того, чтобы
 определить тип возвращаемого значения функции `fn`, которая передается в качестве аргумента:

		```typescript
		(...args: Parameters<T>) => ReturnType<T>
		```

Это означает, что функция, возвращаемая `addLogging`, будет иметь тот же тип возвращаемого
 значения, что и функция `fn`. Таким образом, тип возвращаемого значения сохраняется при
 добавлении логирования к функции.
 //-------------------------------------------------------------------------------
 //-------------------------------------------------------------------------------
 //-------------------------------------------------------------------------------
 //-------------------------------------------------------------------------------
 //-------------------------------------------------------------------------------
 //-------------------------------------------------------------------------------

 *Parameters
 *Parameters
 *Parameters
 *Parameters
 *Parameters

 `Parameters<T>` - это встроенный обобщенный тип в TypeScript, который извлекает типы параметров из типа функции `T` в виде кортежа.

Например, если у вас есть функция с несколькими параметрами:

```typescript
function add(x: number, y: number): number {
    return x + y;
}
```

Вы можете использовать `Parameters` следующим образом:

```typescript
type AddParametersType = Parameters<typeof add>; // [number, number]
```

Таким образом, `AddParametersType` будет кортежем, содержащим типы параметров функции `add`, то есть `[number, number]`.

В контексте вашей функции `addLogging`, `Parameters<T>` используется для определения типов параметров функции `fn`, которая передается в качестве аргумента:

```typescript
(...args: Parameters<T>) => ReturnType<T>
```

Это означает, что функция, возвращаемая `addLogging`, будет принимать те же параметры, что и функция `fn`. Таким образом, типы параметров сохраняются при добавлении логирования к функции.


 */