import {addLogging, addLogging2, addLogging3} from "./logging";

let changeSign = (a: number): number=>-a;

function subtract(a: number, b: number): number{
	if(b === 0){
		throw new Error("Нельзя делить на ноль!");
	}else{
		b = changeSign(b);
		return a + b;
	}
}

if(0){
	//ts-expect-error We want to reassign the function
	// @ts-ignore
	console.log("start")
	const newFunc1 = addLogging(subtract);
	newFunc1(8, 3);
	console.log(); // to separate
	const newFunc2 = addLogging(changeSign);
	newFunc2(15)


	changeSign = addLogging(changeSign);
	newFunc1(7, 5);
	/*
	entering subtract(8,3)
	exiting  subtract=>5

	entering subtract(7,5)
	entering changeSign(5)
	exiting  changeSign=>-5
	exiting  subtract=>2
	*/
}
if(0){
	const subtract2 = addLogging2(subtract);
	try{
		subtract2(11, 0);
	}catch(e){
		console.log(e);
		/* nothing */
	}
	/*
	entering subtract(11,0)
	exiting  subtract=>threw Error: We don't subtract zero!
	*/
}
if(0){
	//ts-expect-error We want to reassign the function
	// @ts-ignore
	console.log("start")
	const newFunc1 = addLogging3(subtract);
	newFunc1(8, 3);
	console.log(); // to separate
	const newFunc2 = addLogging3(changeSign);
	newFunc2(15)

	changeSign = addLogging3(changeSign);
	newFunc1(7, 5);
/*start
entering subtract(8,3)
exiting  subtract=>5

entering changeSign(15)
exiting  changeSign=>-15
entering subtract(7,5)
entering changeSign(5)
exiting  changeSign=>-5
exiting  subtract=>2
*/

}

export {addLogging, addLogging2, subtract};
