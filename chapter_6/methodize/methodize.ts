/*
function methodize(obj, fn) {
  obj.prototype[fn.name] = function (...args) {
    return fn(this, ...args);
  };
}
*/

function methodize<
	O extends { prototype:{ [key:string]:any } },
	T extends any[],
	F extends (arg0:any,...args:T)=>any
>(obj:O,fn:F){
	console.log(fn.name)//reverse
	obj.prototype[fn.name] = function(this:Parameters<F>[0],...args:T):ReturnType<F>{
		console.log("args ",args);
		return fn(this,...args);
	};
}
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
/*
function methodize2(obj, fn) {
  obj.prototype[fn.name] = function(arg){
    return fn(this,arg);
  };
}
*/
function methodize2<
	S extends {prototype:{[key:string]:any}},
	F extends (arg0:any,arg:boolean)=>any
>(obj:S,fn:F){
	obj.prototype[fn.name] = function(this:Parameters<F>[0],arg:Parameters<F>[1]):ReturnType<F>{
		// console.log(this) [ 22, 9, 60, 12, 4, 56 ]
		return fn(this,arg);
	};
}

function methodize3<
	S extends {prototype:{[key:string]:any}},
	F extends (arg0:any)=>any
	>(obj:S,fn:F){
	obj.prototype[fn.name] = function(this:Parameters<F>[0]):ReturnType<F>{
		// console.log(this) [ 22, 9, 60, 12, 4, 56 ]
		return fn(this);
	};
}

export {methodize,methodize2,methodize3};