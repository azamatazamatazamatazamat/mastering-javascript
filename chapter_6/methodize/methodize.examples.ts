import {methodize,methodize2,methodize3} from "./methodize";

declare global{
	interface String{
		reverse(y?:boolean):string;
	}
}

declare global{
	// esl int-disable-next-line @typescript-eslint/no-unused-vars
	interface Array<T>{
		average(this:Array<number>):number;
	}
}
//*************************************************************************
//*************************************************************************
if(0){
	String.prototype.reverse = function(y?:boolean):string{
		const reversed = this.split('').reverse().join('');
		return y ? reversed.toUpperCase() : this.toString(); //this as string;
	}

	const str = "hello";
	console.log(str.reverse(false)); // "hello"
	console.log(str.reverse(true)); // "OLLEH"
}
if(0){
	function reverse(x:string,y = false):string{
		return x.split("").reverse().join(y ? "-" : "");
	}
	methodize2(String,reverse);
	console.log("MONTEVIDEO".reverse(false));//OEDIVETNOM
	console.log("MONTEVIDEO".reverse(true));//O-E-D-I-V-E-T-N-O-M
}
if(1){
	function average(x:number[]):number{
		return (x.reduce((x:number,y:number)=>x + y,0) / x.length);
	}
	methodize3(Array,average);
	console.log([22,9,60,12,4,56].average()); // 27.166667...
	console.log([1,2,3,4].average()); // 2.5
// @ts-expect-error Wrong!
	console.log(["a","b"].average()); // this is not OK
}