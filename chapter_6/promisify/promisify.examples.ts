import {promisify} from "./promisify";
import fs from "fs";
import path from "path";
// or const fs = require("fs");
const innerPath = path.join(__dirname/*process.cwd()*/, "promisify.ts")
	.replace("build","");
		//C:\jWEB\projects\metanit\mastering-javascript\build\chapter_6\promisify\promisify.ts
if(1){
	const cb = (err: any, data: any)=>
		err
			? console.log("ERROR", err)
			: console.log("SUCCESS", data);
	fs.readFile(innerPath, cb); // success, list the data
	fs.readFile("./doesnt_exist.txt", cb); // failure, show exception
}
if(0){
	const badRead = (err: any)=> console.log("UNSUCCESSFUL PROMISE", err);
	const goodRead = (data: any)=> console.log("SUCCESSFUL PROMISE", data);

	const fspromise = promisify(fs.readFile.bind(fs));
	fspromise(innerPath) // success
		.then(goodRead)
		.catch(badRead);

	fspromise("./readmenot.txt") // failure
		.then(goodRead)
		.catch(badRead);
}
