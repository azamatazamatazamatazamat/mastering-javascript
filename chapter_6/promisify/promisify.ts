// VARIADIC TYPES
/*
const promisify = (func) => (...args) =>
  new Promise(
	  (resolve, reject) =>
	  func(...args, (err, data) => err
	    ? reject(err)
	    : resolve(data))
	);

	readFile("./doesnt_exist.txt", cb);
	func(...args, cb)
*/
/**
	Параметры функции promisify
	E: Тип для ошибки, которая может быть передана в колбэк.
	T extends any[]: Массив типов для аргументов функции func.
	D: Тип для данных, которые могут быть переданы в колбэк.

 Объяснение работы функции promisify

    Определение типов: Функция promisify принимает аргумент func, который является функцией,
 		принимающей последний аргумент в виде колбэка (err: E, data: D) => void. Тип T представляет массив
 		типов аргументов, которые принимает функция func.

    Возвращаемая функция: Возвращаемая функция от promisify принимает аргументы типа T (args)
 		и возвращает промис типа D.

    Promise и колбэк: Внутри промиса вызывается функция func с переданными аргументами args.
 		Эта функция func ожидает последний аргумент в виде колбэка (err: E, data: D) => void.

    Обработка колбэка:
        Если колбэк вызывает ошибку (err не равно null или undefined), промис отклоняется с этой
 				ошибкой при помощи reject(err).
        Если колбэк успешно возвращает данные (data), промис разрешается с этими данными при
 				помощи resolve(data).

 */

const promisify = <E, T extends any[], D>(
	func: (...args: [...T, (err: E, data: D)=>void])=>void
)=>{
	return (...args: T): Promise<D>=>{
		// console.log("args>>> ",args)
		// args>>>  ['C:\\jWEB\\projects\\metanit\\mastering-javascript\\\\chapter_6\\promisify\\promisify.ts']
		// args>>>  ['./readmenot.txt']
		return new Promise((resolve, reject)=>{
				return func(...args, (err: E, data: D)=>err
					? reject(err)
					: resolve(data)
				)
			}
		);
	}
}

export {promisify};
