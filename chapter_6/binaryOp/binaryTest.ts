import {binaryLeftOp, binaryOp1, binaryOp2, binaryOpRight} from "./binaryOp";

if(0){
	const myArray = [22, 9, 60, 12, 4, 56];
	console.log(myArray.reduce(binaryOp1("+"), 0));
	console.log(myArray.reduce(binaryOp1("*"), 1));
}
if(0){
	const myArray = [22, 9, 60, 12, 4, 56];
	console.log(myArray.reduce(binaryOp2("+"), 0));
	console.log(myArray.reduce(binaryOp2("*"), 1));
}
if(0){
	const myArray = [22, 9, 60, 12, 4, 56];

	const binaryOp = (op: string)=>{
		return new Function("x", "y", `return x ${op} y;`) as (x: number, y: number)=>number;
	};

	const mySum = myArray.reduce(binaryOp("*"), 1); // Для умножения
	console.log(mySum); // Выводит произведение всех чисел в массиве

	const mySumAddition = myArray.reduce(binaryOp("+"), 0); // Для сложения
	console.log(mySumAddition); // Выводит сумму всех чисел в массиве
}
if(0){

	const binaryOp2 = (op: string)=>{
		return new Function("x", "y", `return x ${op} y;`) as
			(x: number, y: number)=>number;
	}

	const binaryLeftOp = (x: number, op: string)=>{
		return (y: number)=>{
			return binaryOp2(op)(x, y);
		};
	};
	const isNegative1 = binaryLeftOp(0, ">");
	console.log(isNegative1(-1));
	console.log(isNegative1(1));
}
if(0){
	const binaryOp2 = (op: string) => {
		return new Function("x", "y", `return x ${op} y;`) as
			(x: number, y: number) => number;
	}

	const binaryLeftOp = (op: string) => {
		return (x: number, y: number) => {
			return binaryOp2(op)(x, y);
		};
	};

	const myArray = [22, 9, 60, 12, 4, 56];
	const min = myArray.reduce((acc, val) => binaryLeftOp("<")(acc, val) ? acc : val);
	console.log(min);
}