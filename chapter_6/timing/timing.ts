// --------------------------------------------------------
const myGet = (): number=>performance.now();
// --------------------------------------------------------
const myPut = (
	text: string,
	name: string,
	tStart: number,
	tEnd: number
): void=>console.log(`${name} - ${text} ${tEnd - tStart} ms`);

const objGetPut = {
	getTime: myGet,
	output: myPut,
}

// --------------------------------------------------------
function addTiming<T extends (...args: any[])=>any>(
	func: T,
	{getTime, output} = objGetPut
): (...args: Parameters<T>)=>ReturnType<T>{
	return (...args: Parameters<T>): ReturnType<T>=>{
		const tStart = getTime();
		try{
			/** func(...args) - функция, которую возвращаем */
			const valueToReturn = func(...args);
			output("normal exit", func.name, tStart, getTime());
			return valueToReturn;
		}catch(thrownError){
			output("exception!!", func.name, tStart, getTime());
			throw thrownError;
		}
	};
}

// --------------------------------------------------------
export {addTiming};
