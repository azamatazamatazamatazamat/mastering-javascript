import {addTiming} from "./timing";
// -------------------------------------------------------------
function fib(n: number): number {
	return n < 2 ? n : fib(n - 2) + fib(n - 1);
}
// -------------------------------------------------------------
let changeSign = (a: number): number=>-a;
function subtract(a: number, b: number): number{
	b = changeSign(b);
	return a + b;
}
// -------------------------------------------------------------
// @ts-expect-error We want to reassign the function
subtract = addTiming(subtract);
subtract(8, 3);
console.log(); // to separate
changeSign = addTiming(changeSign);
subtract(7, 5);
console.log(); // to separate
/*
subtract - normal exit 0.0217440128326416 ms
changeSign - normal exit 0.0014679431915283203 ms
subtract - normal exit 0.0415341854095459 ms
*/
const myFib = addTiming(fib);
myFib(43);
// fib - normal exit 6053.61829996109 ms
