// const myArray = [22,9,60,12,4,56];
const findOptimum2 = <T>(func:(x:T,y:T)=>T)=>{
	return (arr:T[]):T=>{
		return arr.reduce(func);
	};
};
if(1){
	const findMax = (arr: number[]) => {
		return arr.reduce((x, y) => x > y ? x : y);
	};
	const max = findMax([1, 3, 2, 10, 4, 8]);
	console.log(max); // 10
}
if(0){
	const findOptimum = (arr:number[]):number=>Math.max(...arr);
	console.log(findOptimum([22,9,60,12,4,56])); // 60
}
if(0){
	const returnValue = (value:"max" | "min")=>{
		switch(value){
			case "max":
				return (x:number,y:number):number=>((x>y) ? x : y)
			case "min":
				return (x:number,y:number):number=>((x<y) ? x : y)
		}
	}

	const findMaximum = findOptimum2(returnValue("max"));
	const findMinimum = findOptimum2(returnValue("min"));

	console.log(findMaximum([22,9,60,12,4,56])); // 60
	console.log(findMinimum([22,9,60,12,4,56])); // 4
}
if(0){
//############################################################################
	class Card{
		name:string;
		strength:number;
		powers:number;
		tech:number;

		constructor(n:string,s:number,p:number,t:number){
			this.name = n;
			this.strength = s;
			this.powers = p;
			this.tech = t;
		}
	}

//############################################################################
	const compareHeroes = (
		card1:Card,
		card2:Card
	):Card=>{
		const oneIfBigger = (x:number,y:number):number=>(x>y) ? 1 : 0;

		const wins1 =
			oneIfBigger(card1.strength,card2.strength)+
			oneIfBigger(card1.powers,card2.powers)+
			oneIfBigger(card1.tech,card2.tech);

		const wins2 =
			oneIfBigger(card2.strength,card1.strength)+
			oneIfBigger(card2.powers,card1.powers)+
			oneIfBigger(card2.tech,card1.tech);

		return (wins1>wins2) ? card1 : card2;
	};
//############################################################################
	const codingLeagueOfAmerica = [
		new Card("Electrico",12,21,8),
		new Card("Speediest",8,11,4),
		new Card("Forceful",20,15,2),
		new Card("TechWiz",6,16,30),
	];

	console.log(JSON.stringify(codingLeagueOfAmerica,null,2));

	if(0){
		const findBestHero = findOptimum2(compareHeroes);
		console.log(findBestHero(codingLeagueOfAmerica)); // Electrico is the top Card!
	}
	if(1){

	}
}

export {};
