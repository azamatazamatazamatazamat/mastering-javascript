/*
const unary = func => (...args) => func(args[0]);
*/

const unary = <T extends (...x: any[])=>any>(
	func: T
): ((arg: Parameters<T>[0])=>ReturnType<T>)=>{
	// console.log()
	return (x)=>{
		return func(x);
	}
}

/*
const arity = (n, func) => ()...a) => func(...a.slice(0, n));
*/

function arity<T extends (...args: any[])=>any>(n: number, func: T)
	: (...x: Parameters<T>)=>ReturnType<T>{
	return (...x: Parameters<T>)=>
		func(...x.map((item, index)=>((index < n) ? item : undefined)));
}

export {unary, arity};
