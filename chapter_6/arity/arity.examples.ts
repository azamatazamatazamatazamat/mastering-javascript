import {arity,unary} from "./arity";

if(0){
	const unary2 = <T extends (...x:any[])=>any>(
		func:T
	):((arg:Parameters<T>[2])=>ReturnType<T>)=>{
		// console.log()
		return (x)=>{
			return func(x);
		}
	}
	const logFirst = (a:number,b:boolean,c:string)=>{
		console.log(a);
		return a;
	};

	// console.log(unary2(logFirst)(1)); //
	// console.log(unary2(logFirst)(false)); //
	console.log(unary2(logFirst)("aazzaa")); //
}
if(0){
	console.log(["123.45","-67.8","90"].map(unary(parseInt)));
// [123, -67, 90]0
}
if(0){
	console.log(
		["123.45","-67.8","90"].map((x)=>
			parseInt(x,undefined)
		)
	);
}
if(0){
	const pp = arity(1,parseInt);
	console.log(["123.45","-67.8","90"].map(pp));
// [123, -67, 90]
}
if(1){
	/*
		const arity = (n, func) => {
				return (x) => {
						return !isNaN(parseFloat(x))
							? func(x, 10)
							: undefined;
				};
		};
		const arity = <T extends (x:any,radix:number)=>number | undefined>
		(n:number,func:T)
			:(x:Parameters<T>[0])=>number | undefined=>{

			return (x:Parameters<T>[0]):number | undefined=>{
				return !isNaN(parseFloat(x))
					? func(x,10)
					: undefined;
			}
		}
	*/
	type numUndef = number | undefined
	const arity = <T extends (x:any,radix:number)=>numUndef>
	(n:number,func:T)
		:(x:Parameters<T>[0])=>numUndef=>{

		return (x:Parameters<T>[0]):numUndef=>{
			return !isNaN(parseFloat(x))
				? func(x,10)
				: undefined;
		}

	}

	console.log(
		["123.45","-67.8","90A","A"]
			.map((item:string,index:number)=>
				arity(1,parseFloat)(item))
	);
}



