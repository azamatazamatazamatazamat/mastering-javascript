const demethodize0 = (fn:(..._args:any[])=>any)=>
	(arg0:any)=>
		fn.apply(arg0)

const demethodizeApply = (fn:(..._args:any[])=>any)=>{
	return (arg0:any,..._args:any[])=>{
		return fn.apply(arg0,_args);
	}
}

const demethodizeCall = (fn:(..._args:any[])=>any)=>{
	return (arg0:any)=>{
		return fn.call(arg0);
	}
}

const demethodizeBind = (fn:(..._args:any[])=>any)=>{
	return (arg0:any,..._args:any[])=>{
		return fn.bind(arg0,..._args);
	}
}
// const demethodize = demethodize1;
export {
	demethodize0,
	demethodizeApply,
	demethodizeCall,
	demethodizeBind,
};
