import {
	demethodize0,
	demethodizeApply,
	demethodizeCall,
	demethodizeBind,
} from "./demethodize";

if(0){
	console.log(
		Array
		.prototype
		.sort
		.apply(["delta","alfa","beta","gamma","epsilon"])
	);
}
if(0){
	const sort =  demethodize0(Array.prototype.sort);
	const a = ["delta","alfa","beta","gamma","epsilon"];
	const b = sort(a);
	console.log(a);// мутированный массив
	console.log(b);
}
if(1){
	// const name = "FUNCTIONAL";
	// const result = name.split("").map((x)=>x.toUpperCase());
	// console.log(result);// ["F", "U", "N", "C", "T", "I", "O", "N", "A", "L"]
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	const map = demethodizeApply(Array.prototype.map);
	const toUpperCase = demethodizeCall(String.prototype.toUpperCase);

	const result2 = map("easy",toUpperCase);
	console.log(result2);//[ 'E', 'A', 'S', 'Y' ]

	console.log(
		Array.prototype.map.call("easy",(item)=>{
			return String.prototype.toUpperCase.call(item);
		})
	);//[ 'E', 'A', 'S', 'Y' ]
	console.log(
		Array.prototype.map.apply("easy",[(item)=>{
			return String.prototype.toUpperCase.call(item);
		}])
	);//[ 'E', 'A', 'S', 'Y' ]
}
if(0){
	const toLocaleString = demethodizeBind(Number.prototype.toLocaleString);
	const numbers = [
		2209.6,
		124.56,
		1048576
	];
	// const strings = numbers.map(toLocaleString);
	// for(const string of strings){
	// 	console.log(string())
	// }
	const map = demethodizeApply(Array.prototype.map);
	const strings2 = map(numbers,toLocaleString);
	// console.log(strings2);
	for(const string of strings2){
		console.log(string())
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	const strings3 = Array.prototype.map.call(numbers,(item)=>{
		const newNum = Number.prototype.toLocaleString.bind(item)
		return newNum();
	});
	for(const string of strings3){
		console.log("strings3 >>> ",string);
	}
}
