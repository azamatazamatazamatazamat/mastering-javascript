import {addTiming} from "../timing/timing";
import {
	memoize,
	memoize2,
	memoize3,
	memoize4,
	promiseMemoize,
} from "./memoize";
export function fib(n: number): number{
	return (n == 0) ? 0
		: (n == 1) ? 1
			: fib(n - 2) + fib(n - 1);
}

if(0){
	function factorial(n: number): number{
		if(n === 0)
			return  1

		const total = n + factorial(n - 1);
		console.log("total ",total,"\t",n);
		return total;
	}

	console.log("start")
	const testMemoFact = memoize(factorial);
	console.log(testMemoFact(6));
	console.log(testMemoFact(5));
	console.log(testMemoFact(4));
	console.log(testMemoFact(3));
	console.log(testMemoFact(10));
	/** так как fib -- это рекурсивная функция, мы возвращаем
	 * ей результат прошлых вычислений */
	console.log("----------------------------------")
// @ts-expect-error
// We want to reassign the function
	factorial = memoize(factorial);
	factorial(3);
	factorial(4);
	factorial(6);
	factorial(10);
	factorial(2);
	factorial(5);
}
if(0){
	console.log(">>> - ", fib(10))
	const testFib = (n: number)=>fib(n);

	if(1){
		addTiming(fib)(35); //    146.900 ms
		addTiming(fib)(40); //  1,600.600 ms
		addTiming(fib)(41); // 15,382.255 ms

		addTiming(testFib)(35); //    146.900 ms
		addTiming(testFib)(40); //  1,600.600 ms
		addTiming(testFib)(41); // 15,382.255 ms
	}
	if(0){
		const testMemoFib = memoize(testFib);
		console.log(testMemoFib(14));
		console.log(testMemoFib(14));
		console.log(testMemoFib(15));
		console.log(testMemoFib(13));
		/** так как fib -- это рекурсивная функция, мы возвращаем
		 * ей результат прошлых вычислений */
		console.log("---------------------------------------")
// @ts-expect-error
// We want to reassign the function
		fib = memoize(fib);
		console.log(testMemoFib(14));
		console.log(testMemoFib(14));
		console.log(testMemoFib(15));
		console.log(testMemoFib(13));
	}
	if(0){
		const testMemoFib = memoize(testFib);
		console.log(addTiming(testMemoFib)(41)); // 15,537.575 ms
		console.log(addTiming(testMemoFib)(41)); // 0.005 ms
		console.log(addTiming(testMemoFib)(40)); // 1,368.880 ms
		console.log(addTiming(testMemoFib)(35)); // 123.970 ms
		/** так как fib -- это рекурсивная функция, мы возвращаем
		 * ей результат прошлых вычислений */
		console.log("---------------------------------------")
// @ts-expect-error
// We want to reassign the function
		fib = memoize(fib);
		console.log(addTiming(testMemoFib)(41)); // 0.080 ms
		console.log(addTiming(testMemoFib)(41)); // 0.080 ms
		console.log(addTiming(testMemoFib)(40)); // 0.025 ms
		console.log(addTiming(testMemoFib)(35)); // 0.009 ms
	}
}

