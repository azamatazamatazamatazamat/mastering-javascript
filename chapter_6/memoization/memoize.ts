import {addTiming} from "../timing/timing";
/*
const memoize = (fn) => {
  const cache = {};
  return (x) =>
    x in cache ? cache[x] : (cache[x] = fn(x));
};
*/
// ---------------------------------------------------------------
// const memoize = <T extends (x: number)=>any>(fn: T):
// 	((x: number)=>ReturnType<T>)=>{
// 	const cache = {} as Record<number, ReturnType<T>>;
// 	return (x)=>
// 		x in cache ? cache[x] : (cache[x] = fn(x));
// };
const memoize = <T extends (x: number) => any>(
	func: T
):
	/** возвращаемая функция */
	((x: number) => ReturnType<T>) => {
	/** cache является объектом
	 * ключ - веденное число,
	 * значение - само вычисление*/
	const cache = {} as Record<number, ReturnType<T>>;
//.................................................
	return (x: number): ReturnType<T> => {
		// console.log(JSON.stringify(cache,null,2));
		if (x in cache) {
			return cache[x];
		}
		const result = func(x);
		// console.log(">>> ", func.name);
		cache[x] = result;
		return result;
	};
//.................................................
};
//########################################################################
//########################################################################

const memoize2 = <T extends (x: number, ...y: any[]) => any>(
	fn: T
): ((x: number, ...y: any[]) => ReturnType<T>) => {
	if (fn.length === 1) {
		const cache = {} as Record<number, ReturnType<T>>;
		return (x) =>
			x in cache ? cache[x] : (cache[x] = fn(x));
	} else {
		return fn;
	}
};
//########################################################################
//########################################################################

const memoize3 = <T extends (...x: any[]) => any>(
	fn: T
): ((...x: Parameters<T>) => ReturnType<T>) => {
	const cache = {} as Record<number | string, ReturnType<T>>;
	const PRIMITIVES = ["number", "string"];
	return (...args) => {
		// console.log(typeof JSON.stringify(args),"\t",JSON.stringify(args))// string 	 [2,4]
		const isSingleParameter = (args.length === 1 && PRIMITIVES.includes(typeof args[0]));
		const strX: (number | string) = isSingleParameter ? args[0] : JSON.stringify(args);

		return (strX in cache)
			? cache[strX]
			: (() => {
				const result = fn(...args);
				cache[strX] = result;
				return result;
			})()
	};
};
//########################################################################
//########################################################################

const memoize4 = <T extends (...x: any[]) => any>(
	fn: T
): ((...x: Parameters<T>) => ReturnType<T>) => {
	const cache = {} as Record<string, ReturnType<T>>;
	return (...args) => {
		const strX = JSON.stringify(args);
		return strX in cache
			? cache[strX]
			: (cache[strX] = fn(...args));
	};
};
//########################################################################
//########################################################################

const promiseMemoize = <A, T extends (...x: any[]) => Promise<A>>(
	fn: T
): ((...x: Parameters<T>) => Promise<A>) => {
	const cache = {} as Record<string, Promise<A>>;
	return (...args) => {
		const strX = JSON.stringify(args);
		return strX in cache
			? cache[strX]
			: (() => {
				const result = fn(...args).catch((x) => {
					delete cache[strX];
					return x;
				})
				cache[strX] = result
				return result;
			})();
	};
};

export {
	memoize,
	memoize2,
	memoize3,
	memoize4,
	promiseMemoize,
};
