import {
	once,
	once2,
	onceAndAfter,
	onceAndAfter2,
} from "./once";

const squeak = (x: string) => console.log(x, "squeak!!");
const creak = (x: string) => console.log(x, "creak!!");

if(0){
	const makeSound = once(creak);
	makeSound("door"); //
	makeSound("door"); //
	makeSound("door"); //
	makeSound("door"); //
}
if(0){
	const makeSound = once2(squeak);
	makeSound("door"); //
	makeSound("door"); //
	makeSound("door"); //
	makeSound("door"); //
}
if(0){
	const makeSound = onceAndAfter(creak,squeak);
	makeSound("door1");
	makeSound("door2");
	makeSound("door3");
	makeSound("door4");
}
if(1){
	const makeSound = onceAndAfter2(creak,squeak);
	makeSound("door1");
	makeSound("door2");
	makeSound("door3");
	makeSound("door4");
}
// const makeSound = onceAndAfter2(squeak, creak);
// makeSound("door"); // "door squeak!!"
// makeSound("door"); // "door creak!!"
// makeSound("door"); // "door creak!!"
// makeSound("door"); // "door creak!!"
