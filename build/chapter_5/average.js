"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.markers = void 0;
const myArray = [22, 9, 60, 12, 4, 56];
//-----------------------------------------------------------------------
if (0) {
    const sum = (x, y) => x + y;
    const average = (arr) => arr.reduce(sum, 0) / arr.length;
    console.log(average(myArray)); // 27.166667
}
//-----------------------------------------------------------------------
if (0) {
    const sumOrDivide = (sum, val, ind, arr) => {
        sum += val;
        return (ind == arr.length - 1)
            ? sum / arr.length
            : sum;
    };
    const average2 = (arr) => arr.reduce(sumOrDivide, 0);
    console.log(average2(myArray)); // 27.166667
}
//-----------------------------------------------------------------------
if (0) {
    const average3 = (arr) => {
        const sc = arr.reduce((ac, val) => ({
            sum: val + ac.sum,
            count: ac.count + 1,
        }), { sum: 0, count: 0 });
        return sc.sum / sc.count;
    };
    console.log(average3(myArray)); // 27.166667
}
//-----------------------------------------------------------------------
if (0) {
    const average4 = (arr) => {
        const sc = arr.reduce((ac, val) => {
            const sum = ac[0] + val;
            const value = ac[1] + 1;
            return [sum, value];
        }, [0, 0]);
        return sc[0] / sc[1];
    };
    console.log(Math.round(average4(myArray))); // 27.166667
}
//-----------------------------------------------------------------------
const markers = [
    { name: "AR", lat: -34.6, lon: -58.4 },
    { name: "BO", lat: -16.5, lon: -68.1 },
    { name: "BR", lat: -15.8, lon: -47.9 },
    { name: "CL", lat: -33.4, lon: -70.7 },
    { name: "CO", lat: 4.6, lon: -74.0 },
    { name: "EC", lat: -0.3, lon: -78.6 },
    { name: "PE", lat: -12.0, lon: -77.0 },
    { name: "PY", lat: -25.2, lon: -57.5 },
    { name: "UY", lat: -34.9, lon: -56.2 },
    { name: "VE", lat: 10.5, lon: -66.9 },
];
exports.markers = markers;
//# sourceMappingURL=average.js.map